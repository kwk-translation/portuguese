﻿# TODO: Translation updated at 2017-12-26 12:10

translate portuguese strings:

    # screens.rpy:253
    old "Back"
    new "Back"

    # screens.rpy:254
    old "History"
    new "History"

    # screens.rpy:255
    old "Skip"
    new "Skip"

    # screens.rpy:256
    old "Auto"
    new "Auto"

    # screens.rpy:257
    old "Save"
    new "Save"

    # screens.rpy:258
    old "Q.save"
    new "Q.save"

    # screens.rpy:259
    old "Q.load"
    new "Q.load"

    # screens.rpy:260
    old "Settings"
    new "Settings"

    # screens.rpy:302
    old "New game"
    new "New game"

    # screens.rpy:309
    old "Update Available!"
    new "Update Available!"

    # screens.rpy:320
    old "Load"
    new "Load"

    # screens.rpy:323
    old "Bonus"
    new "Bonus"

    # screens.rpy:329
    old "End Replay"
    new "End Replay"

    # screens.rpy:333
    old "Main menu"
    new "Main menu"

    # screens.rpy:335
    old "About"
    new "About"

    # screens.rpy:337
    old "Please donate!"
    new "Please donate!"

    # screens.rpy:342
    old "Help"
    new "Help"

    # screens.rpy:346
    old "Quit"
    new "Quit"

    # screens.rpy:572
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # screens.rpy:578
    old "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"
    new "Ren'Py version: [renpy.version_only].\n\n[renpy.license!t]"

    # screens.rpy:618
    old "Page {}"
    new "Page {}"

    # screens.rpy:618
    old "Automatic saves"
    new "Automatic saves"

    # screens.rpy:618
    old "Quick saves"
    new "Quick saves"

    # screens.rpy:660
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # screens.rpy:660
    old "empty slot"
    new "empty slot"

    # screens.rpy:677
    old "<"
    new "<"

    # screens.rpy:680
    old "{#auto_page}A"
    new "{#auto_page}A"

    # screens.rpy:683
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # screens.rpy:689
    old ">"
    new ">"

    # screens.rpy:751
    old "Display"
    new "Display"

    # screens.rpy:752
    old "Windowed"
    new "Windowed"

    # screens.rpy:753
    old "Fullscreen"
    new "Fullscreen"

    # screens.rpy:757
    old "Rollback Side"
    new "Rollback Side"

    # screens.rpy:758
    old "Disable"
    new "Disable"

    # screens.rpy:759
    old "Left"
    new "Left"

    # screens.rpy:760
    old "Right"
    new "Right"

    # screens.rpy:765
    old "Unseen Text"
    new "Unseen Text"

    # screens.rpy:766
    old "After choices"
    new "After choices"

    # screens.rpy:767
    old "Transitions"
    new "Transitions"

    # screens.rpy:777
    old "Language"
    new "Language"

    # screens.rpy:783
    old "日本語"
    new "日本語"

    # screens.rpy:784
    old "English"
    new "English"

    # screens.rpy:785
    old "Deutsch"
    new "Deutsch"

    # screens.rpy:788
    old "Français"
    new "Français"

    # screens.rpy:791
    old "Español"
    new "Español"

    # screens.rpy:794
    old "русский"
    new "русский"

    # screens.rpy:807
    old "Text speed"
    new "Text speed"

    # screens.rpy:811
    old "Auto forward"
    new "Auto forward"

    # screens.rpy:818
    old "Music volume"
    new "Music volume"

    # screens.rpy:825
    old "Sound volume"
    new "Sound volume"

    # screens.rpy:831
    old "Test"
    new "Test"

    # screens.rpy:835
    old "Voice volume"
    new "Voice volume"

    # screens.rpy:846
    old "Mute All"
    new "Mute All"

    # screens.rpy:965
    old "The dialogue history is empty."
    new "The dialogue history is empty."

    # screens.rpy:1030
    old "Keyboard"
    new "Keyboard"

    # screens.rpy:1031
    old "Mouse"
    new "Mouse"

    # screens.rpy:1034
    old "Gamepad"
    new "Gamepad"

    # screens.rpy:1047
    old "Enter"
    new "Enter"

    # screens.rpy:1048
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # screens.rpy:1051
    old "Space"
    new "Space"

    # screens.rpy:1052
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # screens.rpy:1055
    old "Arrow Keys"
    new "Arrow Keys"

    # screens.rpy:1056
    old "Navigate the interface."
    new "Navigate the interface."

    # screens.rpy:1059
    old "Escape"
    new "Escape"

    # screens.rpy:1060
    old "Accesses the game menu."
    new "Accesses the game menu."

    # screens.rpy:1063
    old "Ctrl"
    new "Ctrl"

    # screens.rpy:1064
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # screens.rpy:1067
    old "Tab"
    new "Tab"

    # screens.rpy:1068
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # screens.rpy:1071
    old "Page Up"
    new "Page Up"

    # screens.rpy:1072
    old "Rolls back to earlier dialogue."
    new "Rolls back to earlier dialogue."

    # screens.rpy:1075
    old "Page Down"
    new "Page Down"

    # screens.rpy:1076
    old "Rolls forward to later dialogue."
    new "Rolls forward to later dialogue."

    # screens.rpy:1080
    old "Hides the user interface."
    new "Hides the user interface."

    # screens.rpy:1084
    old "Takes a screenshot."
    new "Takes a screenshot."

    # screens.rpy:1088
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # screens.rpy:1094
    old "Left Click"
    new "Left Click"

    # screens.rpy:1098
    old "Middle Click"
    new "Middle Click"

    # screens.rpy:1102
    old "Right Click"
    new "Right Click"

    # screens.rpy:1106
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # screens.rpy:1110
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # screens.rpy:1117
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # screens.rpy:1121
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # screens.rpy:1125
    old "Right Shoulder"
    new "Right Shoulder"

    # screens.rpy:1129
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # screens.rpy:1133
    old "Start, Guide"
    new "Start, Guide"

    # screens.rpy:1137
    old "Y/Top Button"
    new "Y/Top Button"

    # screens.rpy:1140
    old "Calibrate"
    new "Calibrate"

    # screens.rpy:1205
    old "Yes"
    new "Yes"

    # screens.rpy:1206
    old "No"
    new "No"

    # screens.rpy:1252
    old "Skipping"
    new "Skipping"

    # screens.rpy:1473
    old "Menu"
    new "Menu"

    # screens.rpy:1530
    old "Picture gallery"
    new "Picture gallery"

    # screens.rpy:1534
    old "Music player"
    new "Music player"

    # screens.rpy:1543
    old "Musics and pictures"
    new "Musics and pictures"

    # screens.rpy:1508
    old "Bonus chapters"
    new "Bonus chapters"

    # screens.rpy:1511
    old "1 - A casual day at the club"
    new "1 - A casual day at the club"
    
    # screens.rpy:1516
    old "2 - Questioning sexuality (Sakura's route)"
    new "2 - Questioning sexuality (Sakura's route)"
    
    # screens.rpy:1521
    old "3 - Headline news"
    new "3 - Headline news"
    
    # screens.rpy:1526
    old "4a - A picnic at the summer (Sakura's route)"
    new "4a - A picnic at the summer (Sakura's route)"
    
    # screens.rpy:1531
    old "4b - A picnic at the summer (Rika's route)"
    new "4b - A picnic at the summer (Rika's route)"
    
    # screens.rpy:1536
    old "4c - A picnic at the summer (Nanami's route)"
    new "4c - A picnic at the summer (Nanami's route)"
    
    # char profiles
    old "Characters profiles"
    new "Characters profiles"


# TODO: Translation updated at 2019-04-23 11:20

translate portuguese strings:

    # screens.rpy:1541
    old "Opening song lyrics"
    new "Opening song lyrics"

    # screens.rpy:1611
    old "Max le Fou - Taichi's Theme"
    new "Max le Fou - Taichi's Theme"

    # screens.rpy:1613
    old "Max le Fou - Sakura's Waltz"
    new "Max le Fou - Sakura's Waltz"

    # screens.rpy:1615
    old "Max le Fou - Rika's theme"
    new "Max le Fou - Rika's theme"

    # screens.rpy:1617
    old "Max le Fou - Of Bytes and Sanshin"
    new "Max le Fou - Of Bytes and Sanshin"

    # screens.rpy:1619
    old "Max le Fou - Time for School"
    new "Max le Fou - Time for School"

    # screens.rpy:1621
    old "Max le Fou - Sakura's secret"
    new "Max le Fou - Sakura's secret"

    # screens.rpy:1623
    old "Max le Fou - I think I'm in love"
    new "Max le Fou - I think I'm in love"

    # screens.rpy:1625
    old "Max le Fou - Darkness of the Village"
    new "Max le Fou - Darkness of the Village"

    # screens.rpy:1627
    old "Max le Fou - Love always win"
    new "Max le Fou - Love always win"

    # screens.rpy:1629
    old "Max le Fou - Ondo"
    new "Max le Fou - Ondo"

    # screens.rpy:1631
    old "Max le Fou - Happily Ever After"
    new "Max le Fou - Happily Ever After"

    # screens.rpy:1633
    old "J.S. Bach - Air Orchestral suite #3"
    new "J.S. Bach - Air Orchestral suite #3"

    # screens.rpy:1635
    old "Mew Nekohime - TAKE MY HEART (TV Size)"
    new "Mew Nekohime - TAKE MY HEART (TV Size)"

# TODO: Translation updated at 2019-04-27 11:14

translate portuguese strings:

    # screens.rpy:1552
    old "Achievements"
    new "Achievements"

