﻿# TODO: Translation updated at 2017-12-26 12:09

# game/script.rpy:227
translate portuguese splashscreen_73841dfc:

    # centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"
    centered "THIS IS AN ALPHA RELEASE\n\nIt's not meant for public.\nPlease do not distribute!{fast}"

# game/script.rpy:259
translate portuguese konami_code_e5b259c9:

    # "Max le Fou" "What the heck am I doing here?..."
    "Max le Fou" "What the heck am I doing here?..."

# game/script.rpy:295
translate portuguese gjconnect_4ae60ea0:

    # "Disconnected."
    "Disconnected."

# game/script.rpy:324
translate portuguese gjconnect_e254a406:

    # "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."
    "Connection to Game Jolt succesful.\nWARNING: The connection doesn't count for previously saved games."

translate portuguese strings:

    # script.rpy:208
    old "Master"
    new "Master"

    # script.rpy:209
    old "Big brother"
    new "Big brother"

    # script.rpy:280
    old "Disconnect from Game Jolt?"
    new "Disconnect from Game Jolt?"

    # script.rpy:280
    old "Yes, disconnect."
    new "Yes, disconnect."

    # script.rpy:280
    old "No, return to menu."
    new "No, return to menu."

    # script.rpy:342
    old "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"
    new "A problem occured. Maybe your connection has a problem. Or maybe it's Game Jolt derping...\n\nTry again?"

    # script.rpy:342
    old "Yes, try again."
    new "Yes, try again."

    # script.rpy:360
    old "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    new "It seems the authentication failed. Maybe you didn't write correctly the username and token...\n\nTry again?"
    
    # script.rpy:228
    old "Please enter your name and press Enter:"
    new "Please enter your name and press Enter:"
    
    # script.rpy:295
    old "Type here your username and press Enter."
    new "Type here your username and press Enter."
    
    # script.rpy:296
    old "Now, type here your game token and press Enter."
    new "Now, type here your game token and press Enter."
    
    # script.rpy:207
    old "Game Jolt trophy obtained!"
    new "Game Jolt trophy obtained!"
    
    # script.rpy:177
    old "Sakura"
    new "Sakura"
    
    # script.rpy:178
    old "Rika"
    new "Rika"
    
    # script.rpy:179
    old "Nanami"
    new "Nanami"
    
    # script.rpy:180
    old "Sakura's mom"
    new "Sakura's mom"
    
    # script.rpy:181
    old "Sakura's dad"
    new "Sakura's dad"
    
    # script.rpy:182
    old "Toshio"
    new "Toshio"

    # script.rpy:188
    old "Taichi"
    new "Taichi"
    
    # script.rpy:13
    old "{size=80}Thanks for playing!"
    new "{size=80}Thanks for playing!"

# TODO: Translation updated at 2019-04-23 11:20

# game/script.rpy:286
translate portuguese update_menu_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# game/script.rpy:300
translate portuguese gjconnect_dc3e4e02:

    # "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"
    "This functionality is disabled on the Steam version of the game. To use it, use the Game Jolt Version of the game. How did you get here btw? oO"

# TODO: Translation updated at 2019-04-27 11:14

translate portuguese strings:

    # script.rpy:226
    old "Achievement obtained!"
    new "Achievement obtained!"

